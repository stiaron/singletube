<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../app/config/db.php';

$app = new \Slim\App;

require_once '../app/api/users.php';
require_once '../app/api/videos.php';
require_once '../app/api/playlists.php';
require_once '../app/api/auth.php';
require_once '../app/api/upload.php';

$app->run();

?>