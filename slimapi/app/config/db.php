<?php
class db {
  
  public $db;
  
  public function __construct ($username, $pwd) {
    
    $this->username = $username;
    $this->pwd = $pwd;
    
    $this->connect();
  }
  
  public function connect() {
    
    try {
      $this->db =new PDO('mysql:host=127.0.0.1;dbname=singletube',$this->username,$this->pwd);
    }
    catch (PDOException $e) {
        echo "<pre>";
        print("Error connecting to SQL Server.");
        die(print_r($e));
        echo "</pre>";
    }
    
  }
  
}

?>