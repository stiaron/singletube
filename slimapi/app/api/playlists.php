<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$container = $app->getContainer();

/*
 * Vi etablerer en kobling mot databasen. Brukernavn og passord må modifisieres til å passe
 * din egen database.
 * Brukernavn til databasen i dette tilfellet: "root"
 * Passord til databasen i dette tilfellet: ""
*/
$container['connection'] = function() {
    return new db("root", "");
};

// For enkle CORS-spørringer trenger serveren bare legge til følgende headere i responsen:
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// API endepunkt for å hente ut alle spillelister
$app->get('/api/playlist', function () {
  
  // Finn alle spillelister i databasen
  $sql = "SELECT * FROM playlists";
  
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $dbresult = $sth->execute();
  // Hent ut alle spillelistene som en array
  $playlists = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($playlists);
  
});

// API endepunkt for å lage en ny spilleliste
$app->post('/api/playlist/create', function (Request $request, Response $response) {
  
  // Lagre informsjonen vi mottar fra klientsiden
  $uId = $request->getParam('uId');
  $title = $request->getParam('title');
  $description = $request->getParam('description');
  
  // Legg til ny spilleliste i databasen
  $sql = "INSERT INTO playlists (uId, title, description)
          VALUES (:uId, :title, :description)";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Bind parameterne til SQL-statementet
  $sth->bindParam(':uId', $uId);
  $sth->bindParam(':title', $title);
  $sth->bindParam(':description', $description);
  // Utfør query
  $dbresult = $sth->execute();
  // Json encode suksessbeskjed
  echo '{"notice": {"text": "Playlist added"}}';
  
});

// API endepunkt for å hente ut alle spillelister assosiert med en bruker
$app->get('/api/playlist/user/{uId}', function (Request $request, Response $response) {
  
  // Lagre informasjonen vi mottar fra klientsiden
  $uId = $request->getAttribute('uId');
  // Finn alle videoer til en spesifikk bruker
  $sql = "SELECT playlists.pId, playlists.uId, 
          playlists.description, playlists.title, 
          playlists.ts, play_video.vId 
          FROM playlists 
          LEFT JOIN play_video 
          ON playlists.pId=play_video.pId 
          WHERE playlists.uId= $uId 
          GROUP BY playlists.pId 
          ORDER BY playlists.ts DESC";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $dbresult = $sth->execute();
  // Hent ut alle spillelistene som en array
  $playlists = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($playlists);
});

// API endepunkt for å hente ut en spesifikk spilleliste
$app->get('/api/playlist/{id}', function (Request $request, Response $response) {
 
 // Lagre informasjonen vi mottar fra klientsiden
 $id = $request->getAttribute('id');
  // Finn alle spillelister med en spesifikk id
  $sql = "SELECT pId, title, description 
          FROM playlists
          WHERE pId = $id";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $dbresult = $sth->execute();
  // Hent ut alle spillelistene som en array
  $playlists = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($playlists);
});

// API endepunkt for å legge til en video i en spilleliste
$app->post('/api/playlist/addVideo', function (Request $request, Response $response) {
  
  // Lagre informasjonen vi mottar fra klientsiden
  $vId = $request->getParam('vId');
  $pId = $request->getParam('pId');
  
  // Legg til video i spilleliste
  $sql = "INSERT INTO play_video (vId, pId)
          VALUES (:vId, :pId)";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Bind parameterne til SQL-statementet
  $sth->bindParam(':vId', $vId);
  $sth->bindParam(':pId', $pId);
  // Utfør query
  $dbresult = $sth->execute();
  
  // Sjekk om queryen ble gjennomført
  if ($dbresult) {
    // Returner json respons (suksess)
    echo '{"notice": {"text": "Playlist added"}}';
  } else {
    // Returner json respons (feil)
    echo '{"notice": {"error": "Error creating playlist"}}';
  }
  
});

// API endepunkt for å oppdatere en spesifikk spilleliste
$app->put('/api/playlist/update/{id}', function (Request $request, Response $response) {
  
  // Lagre informasjonen vi mottar fra klientsiden
  $id = $request->getAttribute('id');
  $title = $request->getParam('title');
  $description = $request->getParam('description');
  
  // Sett responsen
  $res = array();
  $res[] = $id;
  $res[] = $title;
  $res[] = $description;
  
  // Oppdater spilleliste
  $sql = "UPDATE playlists 
          SET title = :title, description = :description
          WHERE pId = $id";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Bind parameterne til SQL-statementet
  $sth->bindParam(':title', $title);
  $sth->bindParam(':description', $description);
  // Utfør query
  $sth->execute();
  // Json encode arrayen og returner den
  echo json_encode($res);
  
});

// API endepunkt for å hente ut alle videoene i en spesifikk spilleliste
$app->get('/api/playlist/videos/{pId}', function (Request $request, Response $response) { 
  
  // Lagre informasjonen vi mottar fra klientsiden
  $pId = $request->getAttribute('pId');
  
  // Finn alle videoer i en spilleliste
  $sql = "SELECT videos.title, videos.description, 
          videos.vId, videos.thumbnail, play_video.vId, play_video.pId,
          play_video.position
          FROM play_video 
          INNER JOIN videos 
          ON play_video.vId = videos.vId 
          WHERE play_video.pId=:pId
          ORDER BY position";
  
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Bind parameterne til SQL-statementet
  $sth->bindParam(':pId', $pId);
  // Utfør query
  $sth->execute();
  // Hent ut alle spillelistene som en array
  $videos = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($videos);
  
});

// API endepunkt for å endre rekkefølge på videoene i en spilleliste
$app->put('/api/playlist/order/{pId}', function (Request $request, Response $response) {
  
  // Lagre informasjonen vi mottar fra klientsiden
  $pId = $request->getAttribute('pId');
  $data = json_decode($request->getBody());
  
  $res = array();
  
  $i = 0;
  
  // Loop over alle videoene
  foreach($data as $obj) {
    $res[] = $obj->title;
    $res[] = $obj->description;
    $vId = $obj->vId;
    
    // Oppdater rekkefølgen
    $sql = "UPDATE `play_video`
            SET `position` = :position
            WHERE vId = :vId AND pId = :pId";
    // Forbered statementet
    $sth = $this->connection->db->prepare($sql);
    // Bind parameterne til SQL-statementet
    $sth->bindParam(':position', $i);
    $sth->bindParam(':vId', $vId);
    $sth->bindParam(':pId', $pId);
    // Utfør query
    $sth->execute();
    
    $i++;
  }
  
  $res[] = $pId;
  // Json encode arrayen og returner den
  echo json_encode($res);
  
});

?>