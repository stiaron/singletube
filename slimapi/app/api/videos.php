<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$container = $app->getContainer();

// Etablerer en kobling mot databasen. Brukernavn: "root" og passord: "".
$container['connection'] = function() {
    return new db("root", "");
};

// For enkle CORS requests, trenger serveren bare legge til følgende headere i responsen:
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// API endepuntk for å hente ut alle videoer, ordnet etter når de ble opprettet
$app->get('/api/videos', function () {
  
  // Finn alle videoer
  $sql = "SELECT * FROM videos
          ORDER BY ts DESC";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $dbresult = $sth->execute();
  // Hent ut alle spillelistene som en array
  $videos = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($videos);
  
});

// API endepunkt for å hente ut en spesifikk video
$app->get('/api/video/{id}', function (Request $request, Response $response) {
  
  // Lagre informsjonen vi mottar fra klientsiden
  $id = $request->getAttribute('id');
  // Hent ut en spesifikk video
  $sql = "SELECT * FROM videos 
          WHERE vId = $id";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $dbresult = $sth->execute();
  // Hent ut alle spillelistene som en array
  $videos = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($videos);
  
});

// API endepunkt for å hente ut alle videoene til en spesifikk bruker
$app->get('/api/videos/user/{uId}', function (Request $request, Response $response) {
 $uId = $request->getAttribute('uId');
  
  // Hent ut alle videoene til en spesifikk bruker
  $sql = "SELECT * FROM videos 
          WHERE uId = $uId
          ORDER BY ts DESC";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $dbresult = $sth->execute();
  // Hent ut alle spillelistene som en array
  $videos = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($videos);
});

// API endepunkt for å hente ut gjennomsnittlig rating av en konkret video
$app->get('/api/video/rating/{vId}', function (Request $request, Response $response) {
  
  // Lagre informsjonen vi mottar fra klientsiden
  $vId = $request->getAttribute('vId');
  
  // Hent ut gjennomsnittlig rating av en konkret video
  $sql = "SELECT AVG(rating) 
          AS rating
          FROM rating 
          WHERE vId=$vId";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $sth->execute();
  // Hent ut alle spillelistene som en array
  $rating = $sth->fetch();
  // Json encode arrayen og returner den
  echo json_encode($rating);
  
});

// API endepunkt for å legge til en ny rating av en video eller oppdatere en eksisterende en
$app->put('/api/video/rating/add/{vId}', function (Request $request, Response $response) {
  
  // Lagre informsjonen vi mottar fra klientsiden
  $vId = $request->getAttribute('vId');
  $uId = $request->getParam('uId');
  $rating = $request->getParam('rating');
  
  // Legg til video rating
  $query = "SELECT rating
            FROM rating
            WHERE uId = :uId
            AND vId = :vId";
  // Forbered statementet
  $sth = $this->connection->db->prepare($query);
  // Bind parameterne til SQL-statementet
  $sth->bindParam(':uId', $uId);
  $sth->bindParam(':vId', $vId);
  // Utfør query
  $sth->execute();
  // Hent ut alle spillelistene som en array
  $result = $sth->fetchAll(PDO::FETCH_ASSOC);
  
  // Sjekk om en ny rad ble lagt til i databasen
  if ($sth->rowCount() > 0) {
    // Oppdater ratingen
    $sql = "UPDATE rating
            SET rating = :rating
            WHERE uId = :uId
            AND vId = :vId";
    // Forbered statementet
    $sth = $this->connection->db->prepare($sql);
    // Bind parameterne til SQL-statementet
    $sth->bindParam(':rating', $rating);
    $sth->bindParam(':uId', $uId);
    $sth->bindParam(':vId', $vId);
    // Utfør query
    $sth->execute();
  } else {
    // Legg til ny video rating
    $sql = "INSERT INTO rating(uId, vId, rating) 
            VALUES (:uId, :vId, :rating)";
    // Forbered statementet
    $sth = $this->connection->db->prepare($sql);
    // Bind parameterne til SQL-statementet
    $sth->bindParam(':uId', $uId);
    $sth->bindParam(':vId', $vId);
    $sth->bindParam(':rating', $rating);
    // Utfør query
    $sth->execute();
  }
  
  $res = array();
  $res[] = $vId;
  $res[] = $uId;
  $res[] = $rating;
  // Json encode arrayen og returner den
  echo json_encode($res);
  
});

// Hent ut de mest populære videoene
$app->get('/api/videos/popular', function (Request $request, Response $response) {
  
  // Hent ut de mest populære videoene
  $sql = "SELECT rating.vId, title, thumbnail, description, ts, AVG(rating) 
          FROM rating
          INNER JOIN videos 
          ON videos.vId = rating.vId 
          GROUP BY vId ORDER BY AVG(rating) 
          DESC
          LIMIT 10";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $sth->execute();
  // Hent ut alle spillelistene som en array
  $result = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($result);
  
});

// Oppdater informasjon om en konkret video
$app->put('/api/video/update/{vId}', function (Request $request, Response $response) {
  
  // Lagre informsjonen vi mottar fra klientsiden
  $vId = $request->getAttribute('vId');
  $title = $request->getParam('title');
  $description = $request->getParam('description');
  
  $res = array();
  $res[] = $vId;
  $res[] = $title;
  $res[] = $description;
  
  // Oppdater informasjonen om en konkret video
  $sql = "UPDATE `videos`
          SET `title` = :title, `description` = :description
          WHERE vId = :vId";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Bind parameterne til SQL-statementet
  $sth->bindParam(':title', $title);
  $sth->bindParam(':description', $description);
  $sth->bindParam(':vId', $vId);
  // Utfør query
  $sth->execute();
  // Json encode arrayen og returner den
  echo json_encode($res);
});

// API-endepuntk for å hente ut caption filer
$app->get('/api/video/caption/{vId}', function (Request $request, Response $response) {
    
    // Lagre informsjonen vi mottar fra klientsiden
    $vId = $request->getAttribute('vId');
    
    $res = array();
    $res[] = $vId;
    
    // Hent ut alle transkriberingsfilene til videoen
    $sql = "SELECT tId, language, source FROM transcripts WHERE vId=:vId";
    $sth = $this->connection->db->prepare($sql);
    $sth->bindParam(':vId', $vId);
    $sth->execute();
    
    $result = $sth->fetchAll(PDO::FETCH_ASSOC);
    
    echo json_encode($result);
    
});


?>