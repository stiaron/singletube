<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$container = $app->getContainer();

/*
 * Vi etablerer en kobling mot databasen. Brukernavn og passord må modifisieres til å passe
 * din egen database.
 * Brukernavn til databasen i dette tilfellet: "root"
 * Passord til databasen i dette tilfellet: ""
*/
$container['connection'] = function() {
    return new db("root", "");
};

// For enkle CORS requests, trenger serveren bare legge til følgende headere i responsen:
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// API endepunkt for å hente ut alle brukere
$app->get('/api/users', function () {
  
  // Finn alle brukerne
  $sql = "SELECT users.id, users.givenname, users.surname, 
          COUNT(videos.uId) AS totalVideos, 
          COUNT(playlists.uId) AS totalPlaylists 
          FROM users 
          LEFT JOIN videos 
          ON users.id=videos.uId 
          LEFT JOIN playlists 
          ON users.id=playlists.uId 
          WHERE users.user_type=1 
          GROUP BY users.id";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $dbresult = $sth->execute();
  // Hent ut alle spillelistene som en array
  $users = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($users);
  
});

// API endepunkt for å hente ut alle administratorer
$app->get('/api/admins', function () {
  
  // Finn alle administratorer
  $sql = "SELECT givenname, surname, email
          FROM users
          WHERE user_type = 2";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $dbresult = $sth->execute();
  // Hent ut alle spillelistene som en array
  $users = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($users);
  
});

// API endepunkt for å hente ut en spesifikk bruker
$app->get('/api/user/{id}', function (Request $request, Response $response) {
  
  // Lagre informsjonen vi mottar fra klientsiden
  $id = $request->getAttribute('id');
  // Hent ut en enkelt bruker
  $sql = "SELECT * FROM users WHERE id = $id";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $dbresult = $sth->execute();
  // Hent ut alle spillelistene som en array
  $users = $sth->fetchAll(PDO::FETCH_ASSOC);
  // Json encode arrayen og returner den
  echo json_encode($users);
  
});
  
// API endepunkt for å legge til en ny bruker
$app->post('/api/user/create', function (Request $request, Response $response) {
  
  // Lagre informsjonen vi mottar fra klientsiden
  $email = $request->getParam('email');
  $password = $request->getParam('password');
  $firstname = $request->getParam('firstName');
  $surname = $request->getParam('lastName');
  $user_type = $request->getParam('user_type');
  $password = password_hash($password, PASSWORD_DEFAULT);
  
  $res = array();
  
  $res[] = $email;
  $res[] = $password;
  $res[] = $firstname;
  $res[] = $surname;
  $res[] = $user_type;
  
  // Legg til ny bruker
  $sql = 'INSERT INTO users (email, password, givenname, surname, user_type) 
          VALUES (:email, :password, :givenname, :surname, :user_type)';
  // Forbered statementet
  $stmt = $this->connection->db->prepare($sql);
  // Bind parameterne til SQL-statementet
  $stmt->bindParam(':email', $email);
  $stmt->bindParam(':password', $password);
  $stmt->bindParam(':givenname', $firstname);
  $stmt->bindParam(':surname', $surname);
  $stmt->bindParam(':user_type', $user_type);
  // Utfør query
  $stmt->execute();
  
  // Send tilbake responsen
  if ($stmt->rowCount()==0) {
    echo '{"notice": {"error": "The email you\'ve selected already exists"}}';
  } else {
    echo '{"notice": {"text": "User added, please log in"}}';
  }
  
});

// API endepunkt for å slette en spesifikk bruker
$app->delete('/api/user/delete/{uId}', function (Request $request, Response $response) {
  
  // Lagre informsjonen vi mottar fra klientsiden
  $uId = $request->getAttribute('uId');
  
  $res = array();
  
  // Hent ut alle videoene og thumbnail-bildene til brukeren
  $query = "SELECT DISTINCT source, thumbnail FROM videos WHERE uId=:uId";
  // Forbered statementet
  $sth1 = $this->connection->db->prepare($query);
  $sth1->bindParam(':uId', $uId);
  // Utfør query
  $sth1->execute();
  
  if ($sth1->rowCount() > 0) {
    // Fjern alle videoer og thumbnail-bilder fra spilleliste
    while ($row = $sth1->fetch(PDO::FETCH_ASSOC)) {
      unlink("../../app/images/".$row['thumbnail']);
      unlink("../../app/videos/".$row['source']);
    }
  }
  
  // Hent ut alle caption filer til brukeren
  // SELECT source FROM transcripts WHERE 

  // Slett brukeren fra databasen
  $sql = "DELETE FROM users
          WHERE id = :uId";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Bind parameterne til SQL-statementet
  $sth->bindParam(':uId', $uId);
  // Utfør query
  $sth->execute();
  
  // Json encode arrayen og returner den
  echo json_encode($res);
  
});

// API endepunkt for å forfremme en spesifikk bruker
$app->put('/api/user/promote/{id}', function (Request $request, Response $response) {
  
  // Lagre informsjonen vi mottar fra klientsiden
  $id = $request->getAttribute('id');
  
  $res = array();
  $res[] = $id;
  
  // Oppdater bruker_type til admin
  $sql = "UPDATE users 
          SET user_type='1' 
          WHERE id=$id";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Utfør query
  $sth->execute();
  // Json encode arrayen og returner den
  echo json_encode($id);
  
});


