<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$container = $app->getContainer();

/*
 * Vi etablerer en kobling mot databasen. Brukernavn og passord må modifisieres til å passe
 * din egen database.
 * Brukernavn til databasen i dette tilfellet: "root"
 * Passord til databasen i dette tilfellet: ""
*/
$container['connection'] = function() {
    return new db("root", "");
};

// For enkle CORS-spørringer trenger serveren bare legge til følgende headere i responsen:
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// API-endepunkt for å logge brukere inn
$app->post('/api/login', function (Request $request, Response $response) {
  
  // Lagre informasjonen vi mottar fra klientsiden
  $password = $request->getParam("password");
  $email = $request->getParam("email");
  
  $res = array();
  $error = array();
  $token;
  
  // Finn en bruker med en spesifikk email
  $sql = "SELECT id, email, password, user_type
          FROM users 
          WHERE email = :email";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Bind parameterne til SQL-statementet
  $sth->bindParam(':email', $email);
  // Utfør query
  $sth->execute();
  
  // Sjekk om du mottar noen resultater/rader
  if ($rows = $sth->fetch(PDO::FETCH_ASSOC) ) {
    // Sjekk om passordet stemmer med det som ligger i databasen
    if (password_verify($password, $rows['password'])) {
      // Opprett unik token
      $token = md5($rows['id'].(time()));
      // Responsen vi sender tilbake til brukeren
      $res[] = array(
        'username' => $rows['email'],
        'token' => $token,
        'permission' => $rows['user_type'],
        'uId' => $rows['id']
      );
      // Oppdater token hvor email matcher
      $query = "UPDATE users 
                SET token=? 
                WHERE email=?";
      // Forbered statementet
      $stmt = $this->connection->db->prepare($query);
      // Utfør query
      $stmt->execute(array($token, $rows['email']));
      // Json encode arrayen og returner den
      echo json_encode($res);
    } else {
      // Lagre feilmelding i array
      $error[] = array('message' => "The password doesn't match");
      // Json encode arrayen og returner den
      echo json_encode($error);
    }
  } else {
    // Lagre feilmelding i array
    $error[] = array('message' => "Username or password is incorrect");
    // Json encode arrayen og returner den
    echo json_encode($error);
  }
  
});

// API endepunkt for å logge brukern ut
$app->post('/api/logout', function (Request $request, Response $response) {
  
  // Lagre informasjonen vi mottar fra klientsiden
  $token = $request->getParam("token");
  
  // Slett token fra bruker-tabellen når brukeren logger ut
  $sql = "UPDATE users 
          SET token = NULL 
          WHERE token=:token";
  // Forbered statementet
  $sth = $this->connection->db->prepare($sql);
  // Bind parameterne til SQL-statementet
  $sth->bindParam(':token', $token);
  // Utfør query
  $sth->execute();
  // Send suksess tilbake
  echo "success";
  
});


?>