<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$container = $app->getContainer();

/*
 * Vi etablerer en kobling mot databasen. Brukernavn og passord må modifisieres til å passe
 * din egen database.
 * Brukernavn til databasen i dette tilfellet: "root"
 * Passord til databasen i dette tilfellet: ""
*/
$container['connection'] = function() {
    return new db("root", "");
};

// For enkle CORS requests, trenger serveren bare legge til følgende headere i responsen:
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// Dette endepunktet er foreløpig ikke i bruk
$app->post('/api/upload', function (Request $request, Response $response) {
  
  $res = array();
  
  function renameFile ($currentName) {
    $temp = explode(".", $currentName);
    $name = uniqid() . "." . end($temp);
    return $name;
  }

  // Sjekk om en videofil er lastet opp
  if (isset($_FILES['videoFile']['name'])) {
  
    // Lagre dataen vi mottar i variabler
    $uId = $_POST['uId'];
    $title = $_POST['title'];
    $description = $_POST['description'];

    // Lagrer informasjon om thumbnail-filen som er lastet opp
    $thumbnailName = renameFile($_FILES['thumbnailFile']['name']);
    $thumbnailTmp = $_FILES['thumbnailFile']['tmp_name'];
    $thumbnailPath = "../../app/images/";

    // Lagrer informasjon om video-filen som er lastet opp
    $videoName = renameFile($_FILES['videoFile']['name']);
    $videoTmp = $_FILES['videoFile']['tmp_name'];
    $videoPath = "../../app/videos/";

    $res[] = $thumbnailName;
    $res[] = $thumbnailTmp;
    $res[] = $thumbnailPath;
    $res[] = $videoName;
    $res[] = $videoTmp;
    $res[] = $videoPath;

    // Legger videoen inn i databasen
    $sql = "INSERT INTO videos (uId, title, description, source, thumbnail)
            VALUES (:uId, :title, :description, :source, :thumbnail)";
    // Forbered statementet
    $sth = $this->connection->db->prepare($sql);
    // Bind parameterne til SQL-statementet
    $sth->bindParam(':uId', $uId);
    $sth->bindParam(':title', $title);
    $sth->bindParam(':description', $description);
    $sth->bindParam(':source', $videoName);
    $sth->bindParam(':thumbnail', $thumbnailName);
    // Utfør query
    $sth->execute();
    // Lagre den siste id-en som er satt inn i databasen
    $vId = $this->connection->db->lastInsertId();

    // Legg thumbnail-fil som er lastet opp i images-mappen
    move_uploaded_file($thumbnailTmp,$thumbnailPath.$thumbnailName);
    // Legg video-fil som er lastet opp i video-mappen
    move_uploaded_file($videoTmp,$videoPath.$videoName);

    // Sjekk om brukeren har lastet opp en caption-fil
    if (isset($_FILES['captionFiles'])) {
      $langArr = array();

      // Loop over alle språkene som er valgt og legg de inn i array
      foreach ($_POST['langFiles'] as $key => $value) {
        $langArr[] = $_POST['langFiles'][$key];
      }

      $i = 0;

      // Loop over alle caption-filene som er lastet opp.
      foreach ($_FILES['captionFiles']['name'] as $key => $name) {
        // setter fil-informasjonen i variabler
        $captionName = renameFile($_FILES['captionFiles']['name'][$key]);
        $captionTmp = $_FILES['captionFiles']['tmp_name'][$key];
        $captionPath = "../../app/captions/";

        $res[] = $captionName;

        // Legg caption-filen(e) i databasen
        $sql = "INSERT INTO transcripts (vId, language, source) 
                VALUES (:vId, :language, :source)";
        $sth = $this->connection->db->prepare($sql);

        // Bind verdiene du mottar til sql-setninger
        $sth->bindParam(':vId', $vId);
        $sth->bindParam(':language', $langArr[$i]);
        $sth->bindParam(':source', $captionName);
        // Utfør query
        $sth->execute();
        // Legg til caption-fil i riktig mappe
        move_uploaded_file($captionTmp, $captionPath.$captionName);

        $i++;
      }

      // Json encode responsen og returner den
      echo json_encode($res); 
    } else {
      // Json encode responsen og returner den
      echo json_encode($res);
    }

  }
  
});


?>