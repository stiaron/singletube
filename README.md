# Deltakere #
* Mathis Garberg
* Stian Rønningen

# singletube

Dette prosjektet er generert med [yo angular generator](https://github.com/yeoman/generator-angular)
versjon 0.16.0. 

# Setup

1. Klone repsitory inn i htdocs. Husk å starte både MySQL og Apache.
2. Inne i singletube directory med cmd: `yo angular singletube`. 
3. Sett opp som anvist på bildet under: ![yo angular singletube.PNG](https://bitbucket.org/repo/7EK6LKx/images/2574987031-yo%20angular%20singletube.PNG) 
**NB! For hver fil du blir spurt om å overskrive, velg `n`!** 
4. Opprett en fil under db_conn-mappen som heter: `db_info.php`. Dette php skriptet skal inneholde to variabler; `$dbUser` og `$dbPwd` hvor hver av disse inneholder info for ditt MySQL oppsett.
5. Sett opp DB -> `localhost/singletube/db_conn/setup.php`
6. Installer de siste dependenciene ved hjelp av bower: `bower install -g angular-ui-router#1.0.0-rc.1` og `bower install -g ng-file-upload` og `bower install -g angular-route`
7. Installer Slim RESTful API ved hjelp av Composer. Last ned Composer [her.](https://getcomposer.org/download/) Bytt directory til slimapi, f.eks cd slimapi/. Kjør så denne kommandoen: `composer require slim/slim "^3.0"`
8. CD et hakk tilbake, f.eks: `cd ..`
9. skriv `grunt serve` for å få servert websiden vår, singletube. :)

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## Soruces
https://github.com/yeoman/generator-angular

https://www.youtube.com/watch?v=DkOss1ThwYI

https://www.youtube.com/watch?v=RQE0SU3TRqw

http://jasonwatmore.com/post/2014/05/26/angularjs-basic-http-authentication-example

https://www.w3schools.com/angular/angular_validation.asp

https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec

https://www.w3schools.com/angular/angular_validation.asp

https://www.youtube.com/watch?v=DHUxnUX7Y2Y&t=2317s

https://scotch.io/tutorials/sort-and-filter-a-table-using-angular

https://github.com/danialfarid/ng-file-upload

https://github.com/angular-ui/ui-sortable

http://stackoverflow.com/questions/15941592/php-multiple-file-array