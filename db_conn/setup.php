<?php
// hent ut informasjon om brukernavn og passord
require_once('db_info.php');

/*
 * Denne er ansvarlig for å opprette hele databasen. Den går gjennom innholdet i hele
 * sql-filen.
*/

class Database {
  private $pdo;
  
  public function __construct($dbUser, $dbPwd) {
    
    $this->pdo = new PDO ('mysql:host=127.0.0.1', $dbUser, $dbPwd);
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    
    $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'singletube'";
    
    $dbExists = (bool) $this->pdo->query($query)->fetchColumn();
    
    if (!$dbExists) {
      // Create database if not exists
      $this->pdo->query('CREATE DATABASE singletube');
      $this->pdo->query('USE singletube');
      
      $db_sql = file_get_contents(__DIR__ . '/sql/database.sql');
      $this->pdo->exec($db_sql);
    } else {
      $this->pdo->query('USE singletube');
    }
  }
}

// Generate database with specified params
$my_db = new Database($dbUser, $dbPwd);

echo "Database succesfully created!";
?>