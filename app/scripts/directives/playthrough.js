'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.directive:playthrough
 * @description
 * # playthrough
 * Directive of the singletubeApp
 *
 * Directive er grovt sagt en funksjon som kjører når Angular finner en referanse i Dokument Objekt
 * Modellen (DOM). Et directive kan være veldig komplekst, som gjør det vanskelig å forklare
 * hvordan alt fungerer.
 */
angular.module('singletubeApp.directive.playthrough', [])
  .directive('playthrough', function () {
  
    return {
      
      restrict: 'E',
      link: function (scope, element, attributes) {
        
      }
      
    };
    
  });