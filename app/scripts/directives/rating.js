'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.directive:starRating
 * @description
 * # starRating
 * Directive of the singletubeApp
 *
 * Directive er grovt sagt en funksjon som kjører når Angular finner en referanse i Dokument Objekt
 * Modellen (DOM). Et directive kan være veldig komplekst, som gjør det vanskelig å forklare
 * hvordan alt fungerer.
 */
angular.module('singletubeApp.directive.starRating', [])
  .directive('starRating', function () {
    
    /*
     * Dette directivet inneholder mye komplisert logikk, som gjør det vanskelig å forklare.
     * Vi modifiserte et CodePen-eksempel tilgjengelig her:
     * https://codepen.io/AstroDroid/pen/FIdHb
    */
  
    return {
      restrict: 'EA',
      template:
        '<ul class="star-rating" ng-class="{readonly: readonly}">' +
          '<li ng-repeat="star in stars" class="star" ng-class="{filled: star.filled}" ng-click="toggle($index)">' +
            '<i class="fa fa-star"></i>' +
          '</li>' +
        '</ul>',
      scope: {
        ratingValue: '=ngModel',
        max: '=?',
        onRatingSelect: '&?',
        readonly: '=?'
      },
      link: function (scope, element, attributes) {
        if (scope.max == undefined) {
          scope.max = 5;
        }
        function updateStars() {
          scope.stars = [];
          for (var i = 0; i < scope.max; i++) {
            scope.stars.push({
              filled: i < scope.ratingValue
            });
          }
        };
        scope.toggle = function(index) {
          if (scope.readonly == undefined || scope.readonly === false){
            scope.ratingValue = index + 1;
            scope.onRatingSelect({
              rating: index + 1
            });
          }
        };
        scope.$watch('ratingValue', function(oldValue, newValue) {
          if (newValue || newValue === 0) {
            updateStars();
          }
        });
      }
    };
    
  });