'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:CreatePlaylistCtrl
 * @description
 * # CreatePlaylistCtrl
 * Controller of the singletubeApp
 * Template: createPlaylist.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('CreatePlaylistCtrl', function ($scope, $cookies, $location, PlaylistService) {
    
    /*
     * Funksjon som oppretter en ny spilleliste. Vi sender med alle spilleliste-detaljene
     * fra skjemaet som et objekt. Når spillelisten er opprettet logger vi responsen til konsollen
     * og sender brukeren tilbake til hovedsiden.
    */
    $scope.createPlaylist = function (playlistDetails) {
      playlistDetails['uId'] = $cookies.getObject('globals').currentUser.uId;
      PlaylistService.Create(playlistDetails).then(function (notice) {
        console.log(notice);
        $location.path('/main');
      });
    }
    
  });