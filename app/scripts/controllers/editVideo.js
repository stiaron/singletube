'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:EditVideoCtrl
 * @description
 * # EditVideoCtrl
 * Controller of the singletubeApp
 * Template: editVideo.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('EditVideoCtrl', function ($scope, $http, passVidData, VideoService) {
    
    /*
     * Om brukeren aksesserer siden for å endre informasjonen om en video uten å komme
     * direkte fra siden som viser en video vil vi få en feilmelding i konsollen. Under
     * setter vi en default verdi som unngår at feilmeldingen dukker opp.
    */
    if (passVidData.get() !== undefined) {
      $scope.currentVideoData = passVidData.get();
    } else {
      $scope.currentVideoData = "";
    }
    
    /*
     * Funksjon som endrer informasjonen til en video. Vi sender med opplysningene brukeren
     * har skrevet i skjemaet som et objekt.
    */
    $scope.editVideo = function (data) {
      VideoService.Update(data).then(function (res) {
        console.log(res);
      });
    }
  
  });
