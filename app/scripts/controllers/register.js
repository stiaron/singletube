'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the singletubeApp
 * Template: register.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('RegisterCtrl', function ($scope, $http, $location, userService, passVidData) {
    
    /*
     * Funksjon for å opprette en ny bruker. I tilleg til å sende med informasjonen fra
     * skjemaet legger vi inn bruker_type: 0 (standard bruker). Sjekker responsen. Om den ikke
     * inneholder noen feil sender vi brukeren til innloggingssiden.
    */
    $scope.register = function (registerDetails) {
      registerDetails['user_type'] = 0;
      userService.Create(registerDetails).then(function (response) {
        if (!response.notice.error) {
          passVidData.set(response);
          $location.path("/login");
        } else {
          $scope.error = response.notice.error;
        }
      });
    }
  
  });
