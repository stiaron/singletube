'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:VideoCtrl
 * @description
 * # VideoCtrl
 * Controller of the singletubeApp
 * Template: video.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('VideoCtrl', function ($scope, $stateParams, $cookies, VideoService, passVidData, PlaylistService) {
    
    /*
     * Funksjon som henter ut en video med en spesifikk id. Vi sender med videoid som
     * ligger i URL-en som argument.
    */
    VideoService.GetById($stateParams.videoID).then(function (res) {
      $scope.videoDetails = res;
      passVidData.set(res);
    });
    
    VideoService.GetCaptionFiles($stateParams.videoID).then(function (res) {
        $scope.captions = res;
        console.log(res);
    });
    
    /*
     * Funksjon som henter ut alle spillelistene til en spesifikk bruker. Vi sender med
     * brukerid som ligger lagret i et globalt cookie objekt. Sjekker responsen. Sender
     * tilbake meldingen "No playlists." om den er tom.
    */
    PlaylistService.GetByUserId($cookies.getObject('globals').currentUser.uId).then(function (res) {
      if (res.length > 0) {
        $scope.myPlaylists = res;
      } else {
        $scope.message = "No playlists.";
      }
    });
    
    // Vis dropdown menu med oversikt over alle spillelister
    $scope.showAddVideoMenu = function () {
      $scope.videoMenu = true;
    }
    
    /*
     * Funksjon som legger til video i spilleliste. Sjekker om videoen allerede er
     * lagt inn i spillelisten fra før og sender en feilmelding tilbake om dette er tilfellet.
    */
    $scope.addVideoToPlaylist = function (vId, pId) {
      if (pId !== undefined) {
        var data = {vId: vId, pId: pId};
        PlaylistService.Add(data).then(function (res) {
          if (!res.notice.error) {
            $scope.successMsg = res.notice.text;
          } else {
            $scope.errorMsg = res.notice.error;
          }
        });
      } else {
        console.log("Not a playlist!");
      }
    }
    
    // Setter default rating-verdi til 0
    $scope.rating1 = 0;
    
    /*
     * Funksjon som henter ut gjennomsnittlig videorating. Vi sender med videoid som ligger
     * i URL-en som parameter.
    */
    VideoService.GetVideoRating($stateParams.videoID).then(function (res) {
      $scope.rating1 = Math.round(res.rating);
      $scope.averageRating = Math.round(res.rating)
    });
    
    $scope.isReadonly = true;
    
    /*
     * Funksjon som rater en video.
    */
    $scope.rateFunction = function (rating) {
      console.log('Rating selected: ' + rating);
      
      var data = {rating: rating, 
                  vId: $stateParams.videoID, 
                  uId: $cookies.getObject('globals').currentUser.uId}
      
      VideoService.AddVideoRating(data).then(function (res) {
        console.log(res); // logger ut responsen
      });
    }
    
    /* 
     * Funksjon som veksler mellom "2x" og "Normal" hastighet på videoen.
    */
    $scope.video = {
      speed: false,
      video: document.getElementById("video"),
      
      toggleSpeed: function () {
        $scope.video.speed = !$scope.video.speed;
        
        if ($scope.video.speed) {
          video.playbackRate = 2.0;
          video.play();
        } else {
          video.playbackRate = 1.0;
          video.play();
        }
      }
    }
  
  });