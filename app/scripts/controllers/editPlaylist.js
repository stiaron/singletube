'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:EditPlaylistCtrl
 * @description
 * # EditPlaylistCtrl
 * Controller of the singletubeApp
 * Template: editPlaylist.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('EditPlaylistCtrl', function ($scope, $stateParams, $cookies, PlaylistService) {
    
    /*
     * Funksjon for å hente ut en bruker med en spesifikk id. Vi sender med brukerid
     * som ligger lagret i et globalt cookie-objekt. Vi sjekker om responsen returnerer
     * noen resultater. Om den er tom sender vi tilbakemedlingen "No playlists.".
    */
    PlaylistService.GetByUserId($cookies.getObject('globals').currentUser.uId).then(function (res) {
      if (res.length > 0) {
        $scope.myPlaylists = res;
      } else {
        $scope.message = "No playlists.";
      }
    });
    
    /*
     * Funksjon som sjekker om brukeren har valgt en spilleliste i dropdown-menyen. Om en
     * spilleliste er valgt vil vi hente ut informasjonen til spillelisten, samt alle videoene
     * i spillelisten.
    */
    $scope.update = function (id) {
      if(id !== "") {
        PlaylistService.GetByPlaylistId(id).then(function (res) {
        // Oppdater form
        $scope.editPlaylistValues = res;
        });
        PlaylistService.GetVideosInPlaylist(id).then(function (res) {
          $scope.allVideos = res;
        });
      }
    }
    
    /*
     * Funksjon som refresher dropdown-menyen med alle dine spillelister. Funskjonen vil 
     * trigges når du har gjort endringer på en spilleliste, slik at du ser det nye 
     * navnet på spillelisten i dropdown-menyen.
    */
    $scope.refresh = function () {
      PlaylistService.GetByUserId($cookies.getObject('globals').currentUser.uId).then(function (res) {
        if (res.length > 0) {
          $scope.myPlaylists = res;
        } else {
          $scope.message = "No playlists.";
        }
      });
    }
    
    /*
     * Funksjon som oppdaterer informasjonen til en spilleliste.
    */
    $scope.editPlaylist = function (values) {
      PlaylistService.Update(values).then(function (data) {
        console.log(data);
        $scope.refresh();
      });
    }
    
    /*
     * Funksjon som endrer rekkefølgen på videoene i en spilleliste. Du trigger funksjonen ved
     * å "slippe" videoen i en ny eller samme posisjon.
    */
    $scope.sortableOptions = {
      stop: function(e, ui) {
        PlaylistService.EditPlaylistOrder($scope.allVideos).then(function (res) {
          console.log(res);
        });
      }
    };
  
  });
