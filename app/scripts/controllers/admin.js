'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the singletubeApp
 * Template: admin.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('AdminCtrl', function ($scope, $state, userService) {
  
    /*
     * Funksjon som henter ut alle vanlige brukere (studenter). Vi sjekker om responsen
     * returnerer noen resultater. Om den er tom får brukeren tilbakemeldingen "No users.".
    */
    userService.GetAll().then(function (res) {
      if (res.length > 0) {
        $scope.users = res;
        $scope.noUsersMsg = false;
      } else {
        $scope.noUsersMsg = "No users.";
      }
    });
    
    /*
     * Funksjon som henter ut alle administratorer. Vi sjekker om responsen inneholder
     * returnerer noen resultater. Om den er tom får brukeren tilbakemeldingen "No admins.".
    */
    userService.GetAllAdmins().then(function (res) {
      if (res.length > 0) {
        $scope.admins = res;
        $scope.noAdminsMsg = false;
      } else {
        $scope.noAdminsMsg = "No admins.";
      }
    });
    
    /* 
     * Funksjon som refresher tabellen over alle brukere. Vi blir nødt til å oppdatere 
     * tabellen hver gang f.eks. en bruker blir slettet. Om tabellen refreshes og det ikke
     * eksisterer noen brukere får brukeren tilbakemeldingen "No users.".
    */
    $scope.refreshUserTable = function () {
      userService.GetAll().then(function (res) {
        if (res.length > 0) {
        $scope.users = res;
        $scope.noUsersMsg = false;
      } else {
        $scope.noUsersMsg = "No users.";
        $scope.users = false;
      }
      });
    }
    
    /*
     * Funksjon som refresher tabellen over alle administratorer. Vi blir nødt til å 
     * oppdatere grensesnittet hver gang en bruker blir forfremmet til en administrator.
     * Det vil alltid være minst en administrator. Derfor sjekker vi ikke om responsen er tom.
    */
    $scope.refreshAdminTable = function () {
      userService.GetAllAdmins().then(function (res) {
        $scope.admins = res;
        $scope.noAdminsMsg = false;
      });
    }
    
    /*
     * Funksjon som sletter en bruker med en spesifikk bruker id.
    */
    $scope.deleteUser = function (uId) {
      userService.Delete(uId).then(function (res) {
        console.log(res);
        $scope.refreshUserTable();
      });
    }
    
    // Vi setter default sorteringsverdier
    $scope.sortType = 'givenname';
    $scope.sortReverse = false;
    $scope.searchUser = '';
    
    /*
     * Funksjon som forfremmer en bruker med vanlige privilegier til en administrator 
     * med tilgang til admin-panelet. Vi sender med brukerid.
    */
    $scope.promoteUser = function (uId) {
      if (uId) {
        userService.Promote(uId).then(function (res) {
          console.log(res);
          $scope.refreshUserTable();
          $scope.refreshAdminTable();
        });
      }
    }
  
  });