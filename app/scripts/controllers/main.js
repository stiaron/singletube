'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the singletubeApp
 * Template: main.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('MainCtrl', function ($scope, $cookies, VideoService, PlaylistService) {
    $scope.user = $cookies.getObject('globals').currentUser.email;
  
    /*
     * Funksjon som henter ut alle videoene til en bruker med en konkret id. Vi
     * sender med id-en som ligger lagret i det globale cookie objektet. Sjekker responsen.
     * Om den er tom sender vi tilbake meldingen "No videos.".
    */
    VideoService.GetByUserId($cookies.getObject('globals').currentUser.uId).then(function (res) {
      if (res.length > 0) {
        $scope.myVideos = res;
      } else {
        $scope.message1 = "No videos.";
      }
    });
  
    /*
     * Funksjon som henter ut absolutt alle videoer, sortert etter når de ble lagt ut.
     * Sjekker responsen. Om den er tom sender vi tilbake meldingen "No videos.".
    */
    VideoService.GetAll().then(function (result) {
      if (result.length > 0) {
        $scope.newVideos = result;
      } else {
        $scope.message2 = "No videos.";
      }
    });
    
    /*
     * Funksjon som henter ut alle spillelistene til en konkret bruker. Vi sender med id-en
     * som ligger lagret i det globale cookie objektet. Sjekker responsen. Om vi ikke mottar 
     * noen resultater sender vi tilbake meldingen "No playlists".
    */
    PlaylistService.GetByUserId($cookies.getObject('globals').currentUser.uId).then(function (res) {
      if (res.length > 0) {
        $scope.myPlaylists = res;
      } else {
        $scope.message3 = "No playlists.";
      }
    });
    
    /*
     * Funksjon som henter ut alle videoer, sortert etter popularitet/rating. Sjekker responsen.
     * Om vi ikke mottar noen resultater sender vi tilbake medlingen "No videos.".
    */
    VideoService.GetPopularVideos().then(function (res) {
      if (res.length > 0) {
        $scope.popularVideos = res;
      } else {
        $scope.message4 = "No videos.";
      }
    });
    
  });