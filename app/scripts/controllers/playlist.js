'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:PlaylistCtrl
 * @description
 * # PlaylistCtrl
 * Controller of the singletubeApp
 * Template: playlist.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('PlaylistCtrl', function ($scope, $stateParams, VideoService, PlaylistService) {
    
    /*
     * Funksjon som henter ut en video med en spesifikk id. Vi sender med videoID som ligger
     * i URL-en som et parameter.
    */
    VideoService.GetById($stateParams.videoID).then(function (res) {
      $scope.videoDetails = res;
    });
    
    /*
     * Funksjon som henter ut alle videoene i en spilleliste. Vi sender med playlistID som
     * ligger i URL-en som et parameter.
    */
    PlaylistService.GetVideosInPlaylist($stateParams.playlistID).then(function (res) {
      $scope.videosInPlaylist = res;
    });
  
  });