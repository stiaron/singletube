'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the singletubeApp
 * Template: nav.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('LogoutCtrl', function ($scope, $cookies, authenticationService, USER_ROLES) {
    
   /*
    * Denne controlleren er koblet mot navigasjonsmenyen. I navigasjonsmenyen er det en 
    * rekke knapper som skal vises ut fra hvilken privilegier brukeren har. Under kobles
    * tilgangen brukeren har til grensesnittet.
    */
    $scope.userRoles = USER_ROLES;
    $scope.isAuthorized = authenticationService.isAuthorized;
    
    /*
     * Funksjon som tilbakestiller brukerinformasjon og logger brukeren ut.
    */
    $scope.logout = function () {
      authenticationService.ClearCredentials($cookies.getObject('globals').currentUser.authdata);
    }
    
  });