'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the singletubeApp
 * Template: login.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('LoginCtrl', function ($scope, $location, authenticationService, passVidData) {
    
    // Vi setter en default-verdi for kredensialene
    $scope.credentials = {
      username: '',
      password: ''
    };
  
    /*
     * Vi sjekker om brukeren nettopp ble opprettet. Om dette er tilfellet sender vi
     * tilbake en beskjed om at han eller hun må logge seg inn.
    */
    if (passVidData.get().notice) {
      $scope.success = passVidData.get().notice.text;
    }
  
    /*
     * Funksjon for å logge inn en bruker. Vi sender med kredensiale til brukeren som et 
     * objekt. Dette blir i backenden sjekket opp mot opplysningene i databasen. Om innloggingen
     * er vellykket blir kredensialene lagret og brukeren sendt til hovedsiden. Om de ikke stemmer
     * sender vi en feilmelding tilbake.
    */
    $scope.login = function (credentials) {
      authenticationService.Login(credentials, function (response) {
        if (!response.data[0].message) {
          authenticationService.SetCredentials(response.data[0].username, response.data[0].token, response.data[0].permission, response.data[0].uId);
          $location.path("/main");
        } else {
          $scope.error = response.data[0].message;
        }
      });
    }
    
  });