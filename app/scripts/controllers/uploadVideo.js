'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.controller:ContactsCtrl
 * @description
 * # ContactsCtrl
 * Controller of the singletubeApp
 * Template: uploadVideo.html
 *
 * En controller brukes i denne sammenhengen til å gjøre data tilgjengelig for grensesnittet.
 * Vi har opprettet ulike services som gjennomfører de store oppgavene. Ved å inkludere dem som
 * dependencies, blir funksjonene tilgjengelige for oss i controlleren også. Dette bidrar også til
 * at vi følger DRY-prinsippet (Don't Repeat Yourself). Selv om vi ikke går i dybden av funksjonene
 * kommer vi allikevel til å gi en kort beskrivelse av hvordan hver funksjon fungerer.
 */
angular.module('singletubeApp')
  .controller('uploadVideoCtrl', function ($scope, $cookies, $location, Upload, UploadService) {
    
    /*
     * Objekt som innegolder alle språkene som skal være med  i dropdown menyen over 
     * språk til caption-fil.
    */
    $scope.languages = [
      {"id":1, "language": 'English'},
      {"id":2, "language": 'Norwegian'},
      {"id":3, "language": 'Spanish'}
    ];

    /*
     * Funksjonenfor som laster opp en video. Vi sender med detaljene brukeren har fylt inn i
     * skjemaet og legger også til brukerid.
    */
    $scope.uploadVideo = function (videoDetails) {
      videoDetails['uId'] = $cookies.getObject('globals').currentUser.uId;
      UploadService.UploadVideo(videoDetails);
    }
  
  });
