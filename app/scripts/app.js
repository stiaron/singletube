'use strict';

/**
 * @ngdoc overview
 * @name singletubeApp
 * @description
 * # singletubeApp
 *
 * Main module of the application.
 */
angular
  .module('singletubeApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ngFileUpload',
    'singletubeApp.directive.starRating',
    'singletubeApp.directive.playthrough',
    'ui.sortable'
  ])

  // Vi definerer noen konstante verdier vi kommer til å bruke flere steder i applikasjonen.
  .constant('USER_ROLES', {
    all: '*',
    admin: '1',
    student: '0'
  })

  /*
   * Denne delen inneholder en oversikt over alle routes vi har i applikasjonen. Vi har også
   * valgt å dele hver eneste template i 2 forskjellige deler. Den ene delen inneholder
   * navigasjonen, mens den andre delen inneholder innholdet. Årsaken til dette er at det ble
   * vanskelig å knytte funksjonalitet til navigasjonsmenyen når den lå under standard-templaten
   * som kjører i alle templates.
  */
  .config(function ($stateProvider, $urlRouterProvider, USER_ROLES) {
      $stateProvider
      .state('login', {
        url: '/login',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl'
          }
        }
      })
      .state('register', {
        url: '/register',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/register.html',
            controller: 'RegisterCtrl'
          }
        }
      })
      .state('main', {
        url: '/main',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
          }
        }
      })
      .state('createPlaylist', {
        url: '/createPlaylist',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/createPlaylist.html',
            controller: 'CreatePlaylistCtrl'
          }
        }
      })
      .state('uploadVideo', {
        url: '/uploadVideo',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/uploadVideo.html',
            controller: 'uploadVideoCtrl'
          }
        }
      })
      .state('editVideo', {
        url: '/editVideo/:vId',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/editVideo.html',
            controller: 'EditVideoCtrl'
          }
        }
      })
      .state('video', {
        url: '/video/:videoID',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/video.html',
            controller: 'VideoCtrl'
          }
        }
      })
      .state('playlist', {
        url: '/playlist/:playlistID?videoID',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/playlist.html',
            controller: 'PlaylistCtrl'
          }
        }
      })
      .state('editPlaylist', {
        url: '/editPlaylist',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/editPlaylist.html',
            controller: 'EditPlaylistCtrl'
          }
        }
      })
      .state('admin', {
        url: '/admin',
        views: {
          nav: {
            templateUrl: 'views/nav.html',
            controller: 'LogoutCtrl'
          },
          content: {
            templateUrl: 'views/admin.html',
            controller: 'AdminCtrl'
          }
        }
      })
      
      $urlRouterProvider.otherwise('/login');
  })

  .run(function ($rootScope, $location, $cookies, $http, authenticationService, USER_ROLES) {
  
    /*
     * Vi ville i utgangspunktet bruke $stateChangeStart-metoden for å sjekke om brukeren
     * hadde aksessert en ny side. Dette ville gjort det mulig for oss å definere roller
     * for hver eneste template i applikasjonen. Denne måten å gjøre det på var imidlertid
     * utdatert, så vi ble nødt til å finne en annen løsning. Den beskrives kort nedenfor.
    */
  
      // sett en default verdi for globals i tilfelle den er blir udnefined
      $rootScope.globals = $cookies.getObject('globals') || {};
      
      // funksjonen vil kjøre hver gang brukeren endrer lokasjon
      $rootScope.$on('$locationChangeStart', function (event, next, current) {
        
        // sjekk om brukeren forsøker å aksessere avgrensede sier
        var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
        // sjekk om brukeren forsøker å aksessere admin-siden
        var adminPage = $location.path() === '/admin';
        // sjekk login-status
        var loggedIn = $rootScope.globals.currentUser;
        
        // sjekk brukerens tillatelse for å aksessere de avgrensede sidene
        if (adminPage && !authenticationService.isAuthorized(USER_ROLES.admin)) {
          // send brukeren til hovedsiden
          $location.path('/main');
        } else if (restrictedPage && !loggedIn) {
          // send brukeren til login siden
          $location.path('/login');
        }
        
      });
    });
