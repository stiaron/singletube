/**
 * @ngdoc function
 * @name singletubeApp.filter:CheckPlaylist
 * @description
 * # CheckPlaylist
 * Filter of the singletubeApp
 *
 * Et Angular filter formaterer inputen som blir vist til brukeren
 */
angular.module('singletubeApp')
  .filter('CheckPlaylist', function () {
  
  /*
   * Filteret sjekker om spillelisten inneholder noen videoer, og returner et resultat 
   * basert på dette.
  */

  return function(input) {
    
    // Sjekk om spillelisten inneholder en videoID
    if (input.vId) {
      return "<a href='#'>"+input.title+"</a>";
    } else {
      return input.title;
    }
    
  }
  
});