/**
 * @ngdoc function
 * @name singletubeApp.filter:CreatedStatus
 * @description
 * # CreatedStatus
 * Filter of the singletubeApp
 *
 * Et Angular filter formaterer inputen som blir vist til brukeren
 */
angular.module('singletubeApp')
  .filter('exclude', function () {
  
  /*
   * På siden for å opprette en video har du 2 dropdown-menyer for språk. Dette filteret 
   * fjerner det språket du har valgt i den ene dropdown-menyen i den andre.
  */
  
    return function (items, exclude) {
        return items.filter(function (item) {
            return exclude.indexOf(item.language) === -1;
        });
    };
  
});