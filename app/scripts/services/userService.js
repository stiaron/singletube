'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.service:userService
 * @description
 * # userService
 * Service of the singletubeApp
 */
angular.module('singletubeApp')
  .factory('userService', function ($http) {
    var service = {};
    
    // Oversikt over alle metodene i servicen
    service.GetAll = GetAll;
    service.GetAllAdmins = GetAllAdmins;
    service.GetById =  GetById;
    service.Create = Create;
    service.Update = Update;
    service.Delete = Delete;
    service.Promote = Promote;
    
    // Returnerer objektet med alle funksjonene
    return service;
    
    /*
     * Funksjon sender en $http.get request for å hente ut alle vanlige brukere.
    */
    function GetAll () {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/users').then(handleSuccess, handleError('Error getting all users'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut alle administratorer.
    */
    function GetAllAdmins () {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/admins').then(handleSuccess, handleError('Error getting all users'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut en bruker med en spesifikk id.
    */
    function GetById () {
      return $http.get('/api/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
    }
    
    /*
     * Funksjon sender en $http.post request for å opprette en ny bruker.
    */
    function Create(user) {
      return $http.post('http://127.0.0.1/singletube/slimapi/public/api/user/create', user).then(handleSuccess, handleError('Error creating user'));
    }
    
    /*
     * Funksjon sender en 4http.put request for å oppdatere opplysningene til en allerede
     * eksisterende bruker.
    */
    function Update(user) {
      return $http.put('/api/users/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
    }
    
    /*
     * Funksjon sender en $http.delete request for å slette en bruker.
    */
    function Delete(uId) {
      return $http.delete('http://127.0.0.1/singletube/slimapi/public/api/user/delete/' + uId).then(handleSuccess, handleError('Error deleting user'));
    }
    
    /*
     * Funksjon sender en $http.put request for å forfremme en bruker.
    */
    function Promote (id) {
      return $http.put('http://127.0.0.1/singletube/slimapi/public/api/user/promote/' + id).then(handleSuccess, handleError('Error promoting user'));
    }
    
    // returner reponsen
    function handleSuccess (response) {
      return response.data;
    }
    
    // retunrer feilmelding
    function handleError (error) {
      return function () {
        return { success: false, message: error };
      };
    }

  });
