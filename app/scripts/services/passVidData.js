'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.service:passVidData
 * @description
 * # passVidData
 * Service of the singletubeApp
 */
angular.module('singletubeApp')
  .factory('passVidData', function () {
    var savedData = {}
    
    /*
     * Formålet med denne servicen er å kunne sende data mellom to forksjellige templates.
     * Vi bruker denne servicen blant annet til å sende informasjon om at en bruker nettopp
     * er blitt registrert til innloggingssiden som viser beskjeden.
    */
    
    function set(data) {
      savedData = data;
    }
   
    function get() {
      return savedData;
    }

   return {
    set: set,
    get: get
   }

  });
