'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.service:PlaylistService
 * @description
 * # PlaylistService
 * Service of the singletubeApp
 */
angular.module('singletubeApp')
  .factory('PlaylistService', function ($http) {
    var service = {};
    
    // Oversikt over alle metodene i servicen
    service.GetAll = GetAll;
    service.GetByUserId =  GetByUserId;
    service.GetByPlaylistId = GetByPlaylistId;
    service.GetVideosInPlaylist = GetVideosInPlaylist;
    service.EditPlaylistOrder = EditPlaylistOrder;
    service.Create = Create;
    service.Add = Add;
    service.Update = Update;
    service.Delete = Delete;
    
    // Returnerer objektet med alle funksjonene
    return service;
    
    /*
     * Funksjon sender en $http.get request for å hente ut alle spillelistene, ordnet etter
     * når de er opprettet.
    */
    function GetAll () {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/playlist').then(handleSuccess, handleError('Error getting all users'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut alle spillelistene til en konkret
     * bruker, ordnet etter når de ble opprettet.
    */
    function GetByUserId (id) {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/playlist/user/' + id).then(handleSuccess, handleError('Error getting playlist by user id'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut en konkret spilleliste.
    */
    function GetByPlaylistId (id) {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/playlist/' + id).then(handleSuccess, handleError('Error getting playlist by id'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut alle videoene i en spilleliste.
    */
    function GetVideosInPlaylist (pId) {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/playlist/videos/' + pId).then(handleSuccess, handleError('Error getting playlist by id'));
    }
    
    /*
     * Funksjon sender en $http.post request for å opprette en ny spilleliste.
    */
    function Create(playlistData) {
      return $http.post('http://127.0.0.1/singletube/slimapi/public/api/playlist/create', playlistData).then(handleSuccess, handleError('Error creating playlist'));
    }
    
    /*
     * Funksjon sender en $http.post request for å legge en video inn i en spilleliste.
    */
    function Add (data) {
      return $http.post('http://127.0.0.1/singletube/slimapi/public/api/playlist/addVideo', data).then(handleSuccess, handleError('Error adding video to playlist'));
    }
    
    /*
     * Funksjon sender en $http.put request for å oppdatere informasjonen til en eksisterende
     * spilleliste.
    */
    function Update(data) {
      return $http.put('http://127.0.0.1/singletube/slimapi/public/api/playlist/update/' + data.pId, data).then(handleSuccess, handleError('Error updating user'));
    }
    
    /*
     * Funksjon sender en $http.put request for å oppdatere rekkefølgen på videoer i en konkret
     * spilleliste.
    */
    function EditPlaylistOrder (data) {
      return $http.put('http://127.0.0.1/singletube/slimapi/public/api/playlist/order/' + data[0].pId, data).then(handleSuccess, handleError('Error updating user'));
    }
    
    /*
     * Funksjon sender en $http.delete request for å slette en spesifikk spilleliste.
    */
    function Delete(id) {
      return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
    }
    
    // returner responsen
    function handleSuccess (response) {
      return response.data;
    }
    
    // returner feilmelding
    function handleError (error) {
      return function () {
        return { success: false, message: error };
      };
    }

  });
