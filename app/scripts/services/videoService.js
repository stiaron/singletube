'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.service:VideoService
 * @description
 * # VideoService
 * Service of the singletubeApp
 */
angular.module('singletubeApp')
  .factory('VideoService', function ($http) {
    var service = {};
  
    // Oversikt over alle metodene i servicen
    service.GetAll = GetAll;
    service.GetById =  GetById;
    service.GetByUserId = GetByUserId;
    service.GetVideoRating = GetVideoRating;
    service.GetPopularVideos = GetPopularVideos;
    service.GetCaptionFiles = GetCaptionFiles;
    service.Create = Create;
    service.AddVideoRating = AddVideoRating;
    service.Update = Update;
    service.Delete = Delete;
    
    // Returnerer objektet med alle funksjonene
    return service;
    
    /*
     * Funksjon sender en $http.get request for å hente ut alle videoer.
    */
    function GetAll () {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/videos').then(handleSuccess, handleError('Error getting all users'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut en konkret video.
    */
    function GetById (id) {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/video/' + id).then(handleSuccess, handleError('Error getting user by id'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut alle videoene til en konkret bruker.
    */
    function GetByUserId (uId) {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/videos/user/' + uId).then(handleSuccess, handleError('Error getting videos'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut gjennomsnittlig rating til en video.
    */
    function GetVideoRating (vId) {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/video/rating/' + vId).then(handleSuccess, handleError('Error getting videos'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut alle videoer, sortert etter rating.
    */
    function GetPopularVideos () {
      return $http.get('http://127.0.0.1/singletube/slimapi/public/api/videos/popular').then(handleSuccess, handleError('Error getting all users'));
    }
    
    /*
     * Funksjon sender en $http.get request for å hente ut alle transkriberings-filene til en video
    */
    function GetCaptionFiles (vId) {
        return $http.get('http://127.0.0.1/singletube/slimapi/public/api/video/caption/'+vId).then(handleSuccess, handleError('Error getting all captions'));
    }
  
    /*
     * Funksjon sender en $http.post request for å opprette en ny video.
    */
    function Create(user) {
      return $http.post('http://127.0.0.1/singletube/slimapi/public/api/auth/register.php', user).then(handleSuccess, handleError('Error creating user'));
    }
    
    /*
     * Funksjon sender en $http.put request for å legge til ratingen av en video.
    */
    function AddVideoRating (data) {
      return $http.put('http://127.0.0.1/singletube/slimapi/public/api/video/rating/add/' + data.vId, data).then(handleSuccess, handleError('Error creating user'));
    }

    /*
     * Funksjon sender en $http.put request for å oppdatere opplysningene til en eksisterende video.
    */
    function Update(data) {
      return $http.put('http://127.0.0.1/singletube/slimapi/public/api/video/update/' + data.vId, data).then(handleSuccess, handleError('Error updating user'));
    }
    
    /*
     * Funksjon sender en $http.delete request for å slette en spesifikk video.
    */
    function Delete(id) {
      return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
    }
    
    // returner reponsen
    function handleSuccess (response) {
      return response.data;
    }
    
    // retunrer feilmelding
    function handleError (error) {
      return function () {
        return { success: false, message: error };
      };
    }

  });
