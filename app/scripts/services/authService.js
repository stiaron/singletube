'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.service:authenticationService
 * @description
 * # authenticationService
 * Service of the singletubeApp
 */

angular.module('singletubeApp')
  .factory('authenticationService', function ($http, $rootScope, $cookies, $location) {
    var service = {};
    
    // Oversikt over alle funksjonene i servicen
    service.Login = Login;
    service.SetCredentials =  SetCredentials;
    service.ClearCredentials = ClearCredentials;
    service.isAuthorized = isAuthorized;
    
    // Returnerer objektet med alle funksjonene
    return service;
    
    /*
     * Funksjon for å logge inn en bruker. Den aksepterer to argumenter. Det første er
     * brukeropplysningene som er skrevet inn i innloggingsskjemaet. Det andre er en callback-
     * funksjon som kjører på responsen. Vi sender en $http.post request, og sender med
     * brukeropplysningene.
    */
    function Login (authObj, cb) {
      $http.post('http://127.0.0.1/singletube/slimapi/public/api/login', authObj).then(function (response) {
        cb(response);
      });
    }
    
    /*
     * Funksjon for å brukeropplysningene i et globalt objekt, tilgjengelig over hele applikasjonen
    */
    function SetCredentials (email, token, permission, uId) {
      var authdata = token;
      
      // lagre brukerinformasjonen i det globale scopet.
      $rootScope.globals = {
        currentUser: {
          email: email,
          authdata: authdata,
          permission: permission,
          uId: uId
        }
      };
      
      /*
       * Vi lagrere brukerinformasjonen i et globalt cookie-objekt. Brukeren vil være logget inn
       * en uke fremover i tid (eller helt til han/hun logger ut).
      */

      var cookieExp = new Date();
      cookieExp.setDate(cookieExp.getDate() + 7);
      $cookies.putObject('globals', $rootScope.globals, {expires: cookieExp});
      
    }
    
    /*
     * Funksjon som sjekker om brukeren er autorisert til å se innholdet.
    */
    function isAuthorized (authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }
      return ($rootScope.globals.currentUser && authorizedRoles.indexOf($rootScope.globals.currentUser.permission) !== -1)
    }
    
    /*
     * Funksjon som nullstiller kredensialene når brukeren logger ut.
    */
    function ClearCredentials (token) {
      var data = {token: token};
      
      $http.post("http://127.0.0.1/singletube/slimapi/public/api/logout", data).then(function (response) {
        $rootScope.globals = {};
        $cookies.remove('globals');
        // send brukeren tilbake til innloggingssiden
        $location.path("/login");
      });
    }
    
  });
