'use strict';

/**
 * @ngdoc function
 * @name singletubeApp.service:UploadService
 * @description
 * # UploadService
 * Service of the singletubeApp
 */
angular.module('singletubeApp')
  .factory('UploadService', function ($http, Upload) {
    var service = {};
  
    // Oversikt over alle metodene i servicen
    service.UploadVideo = UploadVideo;
    
    // Returnerer objektet med alle funksjonene
    return service;
    
    /*
     * Funksjon sender en request til vår lokale API for å laste opp en video.
     * Vi sender med alle skjema-detaljene brukeren har lagt til.
    */
    function UploadVideo (uploadObj) {
      Upload.upload({
        url: 'http://127.0.0.1/singletube/slimapi/public/api/upload',
        data: { uId: uploadObj.uId,
                title: uploadObj.title,
                description: uploadObj.description,
                videoFile: uploadObj.videoFile,
                thumbnailFile: uploadObj.thumbnailFile,
                captionFiles: { captionFileOne: uploadObj.captionFileOne,
                                captionFileTwo: uploadObj.captionFileTwo},
                langFiles: {langOne: uploadObj.langOne,
                            langTwo: uploadObj.langTwo}}
      }).then(function (res) {
        console.log(res.data);
      });
    }
    
  });
